//
//  ParseKmlMapViewController.swift
//  GoogleMapsTest
//
//  Created by M R on 1/3/18.
//  Copyright © 2018 M R. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit

class ParseKmlMapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        loadKMLFile()
    }
    
    func loadKMLFile () {
        /*
        let path = Bundle.main.path(forResource: "DesertKML", ofType: "kml")
        let url = URL(fileURLWithPath: path!)
        let kmlParser = GMUKMLParser(url: url)
        kmlParser.parse()
        
        let renderer = GMUGeometryRenderer(map: mapView,
                                           geometries: kmlParser.placemarks,
                                           styles: kmlParser.styles)
        
        renderer.render()
        */
    }
}
