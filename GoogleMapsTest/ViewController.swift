//
//  ViewController.swift
//  GoogleMapsTest
//
//  Created by M R on 1/2/18.
//  Copyright © 2018 M R. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController {

    var mapView: GMSMapView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // You don't need to modify the default init(nibName:bundle:) method.
    
    override func loadView() {
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let camera = GMSCameraPosition.camera(withLatitude: 31, longitude: 30.20, zoom: 6.0)
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: 43.991146, longitude: 26.092014)
        marker.title = "Egypt"
        marker.snippet = "Cairo"
        marker.map = mapView
        
        loadKMLFile()
        //loadJsonFile()
    }

    func loadKMLFile () {
        
        let path = Bundle.main.path(forResource: "DesertKML", ofType: "kml")
        let url = URL(fileURLWithPath: path!)
        let kmlParser = GMUKMLParser(url: url)
        kmlParser.parse()
        
        let renderer = GMUGeometryRenderer(map: mapView!,
                                       geometries: kmlParser.placemarks,
                                       styles: kmlParser.styles)
        
        renderer.render()
        
    }
    
    func loadJsonFile () {
        
        let path = Bundle.main.path(forResource: "Desert3", ofType: "geojson")
        let url = URL(fileURLWithPath: path!)
  
        let geoJsonParser = GMUGeoJSONParser(url: url)
        geoJsonParser.parse()
        
        let renderer = GMUGeometryRenderer(map: mapView!, geometries: geoJsonParser.features)
        
        renderer.render()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

